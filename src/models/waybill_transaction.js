const mongoose = require('mongoose');

const waybillTransactionSchema = new mongoose.Schema({
    waybill_no: {
        type: String,
        required: true
    },
    waybill_date: {
        type: Date,
        required: true
    },
    shipper_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Customer'
    },
    consignee_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Customer'
    },
    shipper_address_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Address'
    },
    consignee_address_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Address'
    },
    transaction_type: {
        type: String,
        required: true
    },
    shipment_type: {
        type: String,
        required: true
    },
    total_weight: {
        type: Number,
        required: true
    },
    total_dimension: {
        type: Number,
        required: true
    },
    total_amount: {
        type: Number,
        required: true
    },
    status: {
        type: String,
        required: true
    }
});

const WaybillTransaction = mongoose.model('WaybillTransaction', waybillTransactionSchema);

module.exports = WaybillTransaction;

