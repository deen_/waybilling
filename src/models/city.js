const mongoose = require("mongoose");

const citySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    province_id: {
        type: mongoose.Schema.Types.ObjectId,
        required : true,
        ref: "Province"
    }
});

const City = mongoose.model('City', citySchema);

module.exports = City;