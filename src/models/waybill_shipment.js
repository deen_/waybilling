const mongoose = require('mongoose');

const waybillShipmentSchema = new mongoose.Schema({
    item_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Item'
    },
    quantity: {
        type: Number,
        required: true
    },
    unit_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Unit'
    },
    weight: {
        type: Number
    },
    length: {
        type: Number
    },
    width: {
        type: Number
    },
    height: {
        type: Number
    },
    price: {
        type: Number
    },
    waybill_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'WaybillTransaction'
    }
});

const WaybillShipment = mongoose.model('WaybillShipment', waybillShipmentSchema);

module.exports = WaybillShipment;