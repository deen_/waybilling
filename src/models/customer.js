const mongoose = require('mongoose')

const customerSchema = new mongoose.Schema({
    last_name: {
        type: String,
        required: true
    },
    first_name: {
        type: String,
        require: true
    },
    middle_name: {
        type: String
    },
    email: {
        type: String
    },
    contact_number: {
        type: String
    }
});

const Customer = mongoose.model('Customer', customerSchema)

module.exports = Customer