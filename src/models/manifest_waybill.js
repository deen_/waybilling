const mongoose = require("mongoose");

const manifestWaybillSchema = new mongoose.Schema({
    manifest_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Manifest'
    },
    waybill_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'WaybillTransaction'
    }
});

const ManifestWaybill = mongoose.model('ManifestWaybill', manifestWaybillSchema);

module.exports = ManifestWaybill;