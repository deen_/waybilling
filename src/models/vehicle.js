const mongoose = require("mongoose");

const vehicleSchema = new mongoose.Schema({
    plate_no: {
        type: String,
        required: true
    }
});

const Vehicle = mongoose.model('Vehicle', vehicleSchema);

module.exports = Vehicle;