const mongoose = require("mongoose");

const addressSchema = new mongoose.Schema({
    customer_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Customer'
    },
    barangay_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Barangay"
    },
    street_bldg_room: {
        type: String
    }
});

const Address = mongoose.model('Address', addressSchema);

module.exports = Address;