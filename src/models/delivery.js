const mongoose = require("mongoose");

const deliverySchema = new mongoose.Schema({
    waybill_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "WaybillTransaction"
    },
    barangay_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Barangay"
    },
    street_bldg_room: {
        type: String
    }
});

const Delivery = mongoose.model('Delivery', deliverySchema);

module.exports = Delivery;