const mongoose = require("mongoose");

const barangaySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    city_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    }
});

const Barangay = mongoose.model('Barangay', barangaySchema);

module.exports = Barangay;