const mongoose = require("mongoose");

const manifestSchema = new mongoose.Schema({
    manifest_date: {
        type: Date,
        required: true
    },
    vehicle_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Vehicle"
    },
    driver_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Driver"
    },
    received: {
        type: Boolean,
        default: false
    }
});

const Manifest = mongoose.model('Manifest', manifestSchema);

module.exports = Manifest;

