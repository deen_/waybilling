const WaybillTransaction = require("../models/waybill_transaction")
const Address = require("../models/address")

exports.Customer =  {
    waybills: async({ _id }) => {
        return await WaybillTransaction.find({ $or: [{shipper_id: _id}, {consignee_id: _id}] })
    },
    addresses: async({ _id }) => {
        return await Address.find({ customer_id: _id })
    }
}