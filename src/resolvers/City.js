const Province = require('../models/province')
const Barangay = require('../models/barangay')

exports.City = {
    province: async({ province_id }) => {
        return await Province.findById(province_id)
    },
    barangays: async({ _id }) => {
        return await Barangay.find({ city_id: _id })
    }
}