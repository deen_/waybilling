const WaybillShipment = require("../models/waybill_shipment")

exports.Item = {
    shipments: async({ _id }) => {
        return await WaybillShipment.find({ item_id: _id })
    }
}