const Customer = require("../models/customer")
const Province = require("../models/province")
const City = require("../models/city")
const Barangay = require("../models/barangay")
const Address = require("../models/address")
const Vehicle = require("../models/vehicle")
const Driver = require("../models/driver")
const Unit = require("../models/unit")
const Item = require("../models/item")
const WaybillTransaction = require("../models/waybill_transaction")
const WaybillShipment = require("../models/waybill_shipment")
const Manifest = require("../models/manifest")
const ManifestWaybill = require("../models/manifest_waybill")
const Delivery = require("../models/delivery")

exports.Mutation = {
    addCustomer: async(_, { input }) => {
        const customer = new Customer(input);

        try {
            await customer.save();
            return customer;
        } catch (e) {
            return e;
        }
    },
    addProvince: async(_, { input }) => {
        const province = new Province(input)

        try {
            await province.save();
            return province;
        } catch (e) {
            return e;
        }
    },
    addCity: async(_, { input }) => {
        const city = new City(input)

        try {
            await city.save();
            return city;
        } catch (e) {
            return e;
        }
    },
    addBarangay: async(_, { input }) => {
        const barangay = new Barangay(input);

        try {
            await barangay.save();
            return barangay;
        } catch (e) {
            return e;
        }
    },
    addAddress: async(_, { input }) => {
        const address = new Address(input);
        
        try {
            await address.save();
            return address;
        } catch (e) {
            return e;
        }
    },
    addVehicle: async(_, { plate_no }) => {
        const vehicle = new Vehicle({ plate_no });
        
        try {
            await vehicle.save();
            return vehicle;
        } catch (e) {
            return e;
        }
    },
    addDriver: async(_, { name }) => {
        const driver = new Driver({ name });
        
        try {
            await driver.save();
            return driver;
        } catch (e) {
            return e;
        }
    },
    addUnit: async(_, { name }) => {
        const unit = new Unit({ name })

        try {
            await unit.save();
            return unit;
        } catch (e) {
            return e;
        }
    },
    addItem: async(_, { name }) => {
        const item = new Item({ name });

        try {
            await item.save();
            return item;
        } catch (e) {
            return e;
        }
    },
    addWaybillTransaction: async(_, { input }) => {
        const waybill_transaction = new WaybillTransaction(input);

        try {
            await waybill_transaction.save();
            return waybill_transaction;
        } catch (e) {
            return e;
        }
    },
    addWaybillShipment: async(_, { input }) => {
        const waybill_shipment = new WaybillShipment(input);

        try {
            await waybill_shipment.save();
            return waybill_shipment;
        } catch (e) {
            return e;
        }
    },
    addManifest: async(_, { input }) => {
        const manifest = new Manifest(input);

        try {
            await manifest.save();
            return manifest;
        } catch (e) {
            return e;
        }
    },
    addManifestWaybill: async(_, { input }) => {
        const manifest_waybill = new ManifestWaybill(input);

        try {
            await manifest_waybill.save();
            return manifest_waybill;
        } catch (e) {
            return e;
        }
    },
    addDelivery: async(_, { input }) => {
        const delivery = new Delivery(input);

        try {
            await delivery.save();
            return delivery;
        } catch (e) {
            return e;
        }
    },

    updateCustomer: async(_, { id, input }) => {
        const customer = await Customer.findById(id);
        const updates = Object.keys(input);
        const allowedUpdates = ['first_name', 'last_name', 'middle_name', 'contact_number'];
        
        if ( !filter(updates, allowedUpdates) ) return null;

        try {
            updates.forEach((update) => customer[update] = input[update] )
            await customer.save();
            return customer;
        } catch (e) {
            return e;
        }
    },

    updateProvince: async(_, { id, input }) => {
        const province = await Province.findById(id);
        const updates = Object.keys(input);
        const allowedUpdates = ['name'];

        if ( !filter(updates, allowedUpdates) ) return null;

        try {
            updates.forEach((update) => province[update] = input[update] )
            await province.save();
            return province;
        } catch (e) {
            return e;
        }
    },

    updateCity: async(_, { id, input }) => {
        const city = await City.findById(id);
        const updates = Object.keys(input);
        const allowedUpdates = ["name", "province_id"];

        if ( !filter(updates, allowedUpdates) ) return null;

        try {
            updates.forEach((update) => city[update] = input[update] );
            await city.save()
            return city;
        } catch (e) {
            return e;
        } 
    },

    updateBarangay: async(_, { id, input}) => {
        const barangay = await Barangay.findById(id);
        const updates = Object.keys(input);
        const allowedUpdates = ["name", "city_id"];

        if ( !filter(updates, allowedUpdates) ) return null;

        try {
            updates.forEach((update) => barangay[update] = input[update]);
            await barangay.save();
            return barangay;
        } catch (e) {
            return e;
        }
    },

    updateAddress: async(_, { id, input }) => {
        const address = await Address.findById(id);
        const updates = Object.keys(input);
        const allowedUpdates = ['street_bldg_room', 'barangay_id'];

        if ( !filter(updates, allowedUpdates) ) return null;

        try {
            updates.forEach((update) => address[update] = input[update]);
            await address.save();
            return address;
        } catch (e) {
            return e;
        }
    },

    updateVehicle: async(_, { id, plate_no }) => {
        const vehicle = await Vehicle.findById(id);

        try {
            vehicle["plate_no"] = plate_no;
            await vehicle.save();
            return vehicle;
        } catch (e) {
            return e;
        }
    },

    updateDriver: async(_, { id, name }) => {
        const driver = await Driver.findById(id);

        try {
            driver["name"] = name;
            await driver.save();
            return driver;
        } catch (e) {
            return e;
        } 
    },

    updateUnit: async(_, { id, name }) => {
        const unit = await Unit.findById(id);

        try {
            unit["name"] = name;
            await unit.save();
            return unit;
        } catch (e) {
            return e;
        }
    },

    updateItem: async(_, { id, name }) => {
        const item = await Item.findById(id);
        try {
            item["name"] = name;
            await item.save();
            return item;
        } catch (e) {
            return e;
        }
    },

    updateWaybillTransaction: async(_, { id, input }) => {
        const waybill_transaction = await WaybillTransaction.findById(id);
        const updates = Object.keys(input);
        const allowedUpdates = [
            'shipper_id', 
            'consignee_id', 
            'shipper_address_id', 
            'consignee_address_id', 
            'transaction_type',
            'shipment_type',
            'total_weight',
            'total_dimension',
            'total_amount',
            'status'
        ]

        if ( !filter(updates, allowedUpdates) ) return null;

        try {
            updates.forEach((update) => waybill_transaction[update] = input[update]);
            await waybill_transaction.save();
            return waybill_transaction;
        } catch (e) {
            return e;
        }
    },

    updateWaybillShipment: async(_, { id, input }) => {
        const waybill_shipment = await WaybillShipment.findById(id);
        const updates = Object.keys(input);
        const allowedUpdates = ['item_id', 'quantity', 'unit_id', 'weight', 'length', 'width', 'height', 'price'];
    
        if ( !filter(updates, allowedUpdates) ) return null;

        try {
            updates.forEach((update) => waybill_shipment[update] = input[update]);
            await waybill_shipment.save();
            return waybill_shipment;
        } catch (e) {
            return e;
        }
    },

    updateManifest: async(_, { id, input }) => {
        const manifest = await Manifest.findById(id);
        const updates = Object.keys(input);
        const allowedUpdates = ['driver_id', 'vehicle_id', 'received'];

        if ( !filter(updates, allowedUpdates) ) return null;

        try {
            updates.forEach((update) => manifest[update] = input[update]);
            await manifest.save();
            return manifest;
        } catch (e) {
            return e;
        }
    },

    updateManifestWaybill: async(_, { id, waybill_id }) => {
        const manifest_waybill = await ManifestWaybill.findById(id);

        try {
            manifest_waybill['waybill_id'] = waybill_id;
            await manifest_waybill.save();
            return manifest_waybill;
        } catch (e) {
            return e;
        }
    },

    updateDelivery: async(_, { id, input }) => {
        const delivery = await Delivery.findById(id);
        const updates = Object.keys(input);
        const allowedUpdates = ['street_bldg_room', 'barangay_id'];

        if ( !filter(updates, allowedUpdates) ) return null;

        try {
            updates.forEach((update) => delivery[update] = input[update]);
            await delivery.save();
            return delivery;
        } catch (e) {
            return e;
        }
    },


    deleteWaybillTransaction: async(_, { id }) => {
        try {
            const waybill = await WaybillTransaction.findByIdAndDelete(id);
            const shipments = await WaybillShipment.deleteMany({ waybill_id: id });
            const delivery = await Delivery.deleteMany({ waybill_id: id });
            const manifest_waybill = await ManifestWaybill.deleteMany({ waybill_id: id });
            
            if ( !waybill || !shipments || !delivery || !manifest_waybill ) return false;
    
            return true;
        } catch (e) {
            return false;
        }
    },

    deleteWaybillShipment: async(_, { id }) => {
        try {
            const shipment = await WaybillShipment.findByIdAndDelete(id);
            
            if ( !shipment ) return false;

            return true;
        } catch (e) {
            return false;
        }
    },

    deleteManifest: async(_, { id }) => {
        try {
            const manifest = await Manifest.findByIdAndDelete(id);
            const waybills = await ManifestWaybill.deleteMany({ manifest_id: id });

            if ( !manifest || !waybills ) return false;

            return true;
        } catch (e) {
            return false;
        }
    },

    deleteManifestWaybill: async(_, { id }) => {
        try {
            const manifest_waybill = await ManifestWaybill.findByIdAndDelete(id);

            if ( !manifest_waybill ) return false;

            return true;
        } catch (e) {
            return false;
        }
    },

    deleteDelivery: async(_, { id }) => {
        try {
            const delivery = await Delivery.findByIdAndDelete(id);

            if ( !delivery ) return false;

            return true;
        } catch (e) {
            return false;
        }
    }
}

function filter(updates, allowedUpdates) {
    const isValid = updates.every((update) => allowedUpdates.includes(update));
    return isValid;
}