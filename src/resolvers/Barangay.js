const City = require('../models/city');

exports.Barangay = {
    city: async({ city_id }) => {
        return await City.findById(city_id)
    }
}