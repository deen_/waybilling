const Manifest = require("../models/manifest")

exports.Driver = {
    manifests: async({ _id }) => {
        return await Manifest.find({ driver_id: _id })
    }
}