const Customer = require('../models/customer')
const Barangay = require('../models/barangay')

exports.Address = {
    customer: async({ customer_id }) => {
        return await Customer.findById( customer_id );
    },
    barangay: async({ barangay_id }) => {
        return await Barangay.findById( barangay_id );
    }
}