const Item = require('../models/item')
const Unit = require('../models/unit')
const WaybillTransaction = require('../models/waybill_transaction')

exports.WaybillShipment = {
    item: async({ item_id }) => {
        return await Item.findById(item_id);
    },
    unit: async({ unit_id }) => {
        return await Unit.findById(unit_id);
    },
    waybill: async({ waybill_id }) => {
        return await WaybillTransaction.findById(waybill_id)
    }
}