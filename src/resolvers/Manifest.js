const Driver = require("../models/driver")
const Vehicle = require("../models/vehicle")
const ManifestWaybill = require("../models/manifest_waybill")

exports.Manifest = {
    driver: async({ driver_id }) => {
        return await Driver.findById(driver_id)
    },
    vehicle: async({ vehicle_id }) => {
        return await Vehicle.findById(vehicle_id)
    },
    waybills: async({ _id }) => {
        return await ManifestWaybill.find({ manifest_id: _id })
    }
}
