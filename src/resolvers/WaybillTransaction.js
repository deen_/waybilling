const Customer = require('../models/customer')
const Address = require('../models/address')
const WaybillShipment = require('../models/waybill_shipment')
const Delivery = require('../models/delivery')

exports.WaybillTransaction = {
    shipper: async({ shipper_id }) => {
        return await Customer.findById(shipper_id)
    },
    consignee: async({ consignee_id }) => {
        return await Customer.findById(consignee_id)
    },
    shipper_address: async({ shipper_address_id }) => {
        return await Address.findById(shipper_address_id)
    },
    consignee_address: async({ shipper_address_id }) => {
        return await Address.findById(shipper_address_id)
    },
    shipments: async({ _id }) => {
        return await WaybillShipment.find({ waybill_id: _id })
    },
    delivery: async({ _id }) => {
        console.log(_id)
        return await Delivery.findOne({ waybill_id: _id })
    }
}