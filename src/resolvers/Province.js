const City = require("../models/city")

exports.Province = {
    cities: async({ _id }) => {
        return await City.find({ province_id: _id })
    }
}