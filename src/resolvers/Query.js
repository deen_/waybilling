const Customer = require('../models/customer')
const Province = require('../models/province')
const City = require('../models/city')
const Barangay = require('../models/barangay')
const Address = require('../models/address')
const Vehicle = require('../models/vehicle')
const Driver = require('../models/driver')
const Unit = require('../models/unit')
const Item = require('../models/item')
const WaybillTransaction = require('../models/waybill_transaction')
const WaybillShipment = require('../models/waybill_shipment')
const Manifest = require('../models/manifest')
const ManifestWaybill = require('../models/manifest_waybill')
const Delivery = require('../models/delivery')

exports.Query = {
    customers: async() => {
        return await Customer.find();
    },
    provinces: async() => {
        return await Province.find();
    },
    cities: async() => {
        return await City.find();
    },
    barangays: async() => {
        return await Barangay.find();
    },
    addresses: async() => {
        return await Address.find()
    },
    vehicles: async() => {
        return await Vehicle.find()
    },
    drivers: async() => {
        return await Driver.find()
    },
    units: async() => {
        return await Unit.find()
    },
    items: async() => {
        return await Item.find()
    },
    waybill_transactions: async() => {
        return await WaybillTransaction.find()
    },
    waybill_shipments: async() => {
        return await WaybillShipment.find()
    },
    manifests: async() => {
        return await Manifest.find()
    },
    manifest_waybills: async() => {
        return await ManifestWaybill.find()
    },
    deliveries: async() => {
        return await Delivery.find()
    }
}