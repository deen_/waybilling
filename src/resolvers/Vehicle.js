const Manifest = require("../models/manifest")

exports.Vehicle = {
    manifests: async({ _id }) => {
        return await Manifest.find({ vehicle_id: _id })
    }
}