const Manifest = require("../models/manifest")
const WaybillTransaction = require("../models/waybill_transaction")

exports.ManifestWaybill = {
    manifest: async({ manifest_id }) => {
        return await Manifest.findById(manifest_id)
    },
    waybill: async({ waybill_id }) => {
        return await WaybillTransaction.findById(waybill_id)
    }
}