const WaybillTransaction = require("../models/waybill_transaction")
const Barangay = require("../models/barangay")

exports.Delivery = {
    waybill: async({ waybill_id }) => {
        return await WaybillTransaction.findById(waybill_id)
    },
    barangay: async({ barangay_id }) => {
        return await Barangay.findById(barangay_id)
    }
}