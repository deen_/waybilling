const { gql } = require("apollo-server");

exports.typeDefs = gql`
    type Query {
        customers: [Customer!]
        provinces: [Province!]
        cities: [City!]
        barangays: [Barangay!]
        addresses: [Address!]
        vehicles: [Vehicle!]
        drivers: [Driver!]
        units: [Unit!]
        items: [Item!]
        waybill_transactions: [WaybillTransaction!]
        waybill_shipments: [WaybillShipment!]
        manifests: [Manifest!]
        manifest_waybills: [ManifestWaybill!]
        deliveries: [Delivery!]
    }

    type Mutation {
        addCustomer(input: AddCustomerInput): Customer
        addProvince(input: ProvinceInput): Province
        addCity(input: AddCityInput): City
        addBarangay(input: AddBarangayInput): Barangay
        addAddress(input: AddAddressInput): Address
        addVehicle(plate_no: String!): Vehicle
        addDriver(name: String!): Driver
        addUnit(name: String): Unit
        addItem(name: String!): Item
        addWaybillTransaction(input: AddWaybillTransactionInput): WaybillTransaction
        addWaybillShipment(input: AddWaybillShipmentInput): WaybillShipment
        addManifest(input: AddManifestInput): Manifest
        addManifestWaybill(input: AddManifestWaybillInput): ManifestWaybill
        addDelivery(input: AddDeliveryInput): Delivery

        updateCustomer(id: ID!, input: UpdateCustomerInput): Customer
        updateProvince(id: ID!, input: ProvinceInput): Province
        updateCity(id: ID!, input: UpdateCityInput): City
        updateBarangay(id: ID!, input: UpdateBarangayInput): Barangay
        updateAddress(id: ID!, input: UpdateAddressInput): Address
        updateVehicle(id: ID, plate_no: String): Vehicle
        updateDriver(id: ID!, name: String): Driver
        updateUnit(id: ID!, name: String): Unit
        updateItem(id: ID!, name: String): Item
        updateWaybillTransaction(id: ID!, input: UpdateWaybillTransactionInput): WaybillTransaction
        updateWaybillShipment(id: ID!, waybill_id: ID!): WaybillShipment
        updateManifest(id: ID!, input: UpdateManifestInput): Manifest
        updateManifestWaybill(id: ID!, waybill_id: ID!): ManifestWaybill
        updateDelivery(id: ID!, input: UpdateDeliveryInput): Delivery
        
        deleteWaybillTransaction(id: ID!): Boolean!
        deleteWaybillShipment(id: ID!): Boolean!
        deleteManifest(id: ID!): Boolean!
        deleteManifestWaybill(id: ID!): Boolean!
        deleteDelivery(id: ID!): Boolean!
    }

    type Customer {
        id: ID!
        first_name: String!
        last_name: String!
        middle_name: String
        email: String
        contact_number: String
        waybills: [WaybillTransaction!]
        addresses: [Address!]
    }

    type Province {
        id: ID!
        name: String!
        cities: [City!]
    }

    type City {
        id: ID!
        name: String!
        province: Province!
        barangays: [Barangay!]
    }

    type Barangay {
        id: ID!
        name: String!
        city: City
    }

    type Address {
        id: ID!
        street_bldg_room: String
        barangay: Barangay!
        customer: Customer!
    }

    type Vehicle {
        id: ID!
        plate_no: String!
        manifests: [Manifest!]
    }

    type Driver {
        id: ID!
        name: String!
        manifests: [Manifest!]
    }

    type Unit {
        id: ID!
        name: String!
    }

    type Item {
        id: ID!
        name: String!
        shipments: [WaybillShipment!]
    }

    type WaybillTransaction {
        id: ID!
        waybill_no: String!
        waybill_date: String!
        shipper: Customer!
        consignee: Customer!
        shipper_address: Address!
        consignee_address: Address!
        transaction_type: TransactionType!
        shipment_type: ShipmentType!
        total_weight: Float
        total_dimension: Float
        total_amount: Float
        status: Status!
        shipments: [WaybillShipment!]!
        delivery: Delivery
    }

    type WaybillShipment {
        id: ID!
        item: Item!
        quantity: Int
        unit: Unit!
        weight: Float
        length: Float
        width: Float
        height: Float
        price: Float
        waybill: WaybillTransaction!
    }

    type Manifest {
        id: ID!
        manifest_date: String!
        vehicle: Vehicle!
        driver: Driver!
        received: Boolean
        waybills: [ManifestWaybill!]
    }

    type ManifestWaybill {
        id: ID!
        manifest: Manifest!
        waybill: WaybillTransaction!
    }

    type Delivery {
        id: ID!
        waybill: WaybillTransaction!
        barangay: Barangay!
        street_bldg_room: String!
    }

    enum TransactionType {
        PREPAID
        CHARGE
        COLLECT
    }

    enum ShipmentType {
        BREAKABLE
        PERISHABLE
        LETTER
        OTHERS
    }

    enum Status {
        ACCEPTED
        MANIFESTED
        ARRIVED AT DESTINATION
        FOR DELIVERY
        DELIVERED
        RELEASED
    }

    input AddCustomerInput {
        first_name: String!
        last_name: String!
        middle_name: String
        email: String!
        contact_number: String!
    }

    input UpdateCustomerInput {
        first_name: String
        last_name: String
        middle_name: String
        contact_number: String
    }

    input ProvinceInput {
        name: String!
    }

    input AddCityInput {
        name: String!
        province_id: ID!
    }

    input UpdateCityInput {
        name: String
        province_id: ID
    }

    input AddBarangayInput {
        name: String!
        city_id: ID!
    }

    input UpdateBarangayInput {
        name: String
        city_id: ID
    }

    input AddAddressInput {
        street_bldg_room: String!
        barangay_id: ID!
        customer_id: ID!
    }

    input UpdateAddressInput {
        street_bldg_room: String
        barangay_id: ID
    }

    input AddWaybillTransactionInput {
        waybill_no: String!
        waybill_date: String!
        shipper_id: ID!
        consignee_id: ID!
        shipper_address_id: ID!
        consignee_address_id: ID!
        transaction_type: TransactionType!
        shipment_type: ShipmentType!
        total_weight: Float
        total_dimension: Float
        total_amount: Float
        status: Status!
    }

    input UpdateWaybillTransactionInput {
        shipper_id: ID
        consignee_id: ID
        shipper_address_id: ID
        consignee_address_id: ID
        transaction_type: TransactionType
        shipment_type: ShipmentType
        total_weight: Float
        total_dimension: Float
        total_amount: Float
        status: Status
    }

    input AddWaybillShipmentInput {
        item_id: ID!
        quantity: Int
        unit_id: ID!
        weight: Float
        length: Float
        width: Float
        height: Float
        price: Float
        waybill_id: ID!
    }

    input UpdateWaybillShipmentInput {
        item_id: ID
        quantity: Int
        unit_id: ID
        weight: Float
        length: Float
        width: Float
        height: Float
        price: Float
    }

    input AddManifestInput {
        manifest_date: String!
        driver_id: ID!
        vehicle_id: ID!
        received: Boolean
    }

    input UpdateManifestInput {
        driver_id: ID
        vehicle_id: ID
        received: Boolean
    }

    input AddManifestWaybillInput {
        manifest_id: ID!
        waybill_id: ID!
    }

    input AddDeliveryInput {
        street_bldg_room: String!
        barangay_id: ID!
        waybill_id: ID!
    }

    input UpdateDeliveryInput {
        street_bldg_room: String
        barangay_id: ID
    }
`