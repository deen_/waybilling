const { Query } = require("../resolvers/Query")
const { Mutation } = require("../resolvers/Mutation")
const { Customer } = require("../resolvers/Customer")
const { Province } = require("../resolvers/Province")
const { City } = require("../resolvers/City")
const { Barangay } = require("../resolvers/Barangay")
const { Address } = require("../resolvers/Address")
const { Vehicle } = require("../resolvers/Vehicle")
const { Driver } = require("../resolvers/Driver")
const { Item } = require("../resolvers/Item")
const { WaybillTransaction } = require("../resolvers/WaybillTransaction")
const { WaybillShipment } = require("../resolvers/WaybillShipment")
const { Manifest } = require("../resolvers/Manifest")
const { ManifestWaybill } = require("../resolvers/ManifestWaybill")
const { Delivery } = require("../resolvers/Delivery")

const resolvers = {
    Query,
    Mutation,
    Customer,
    Province,
    City,
    Barangay,
    Address,
    Vehicle,
    Driver,
    Item,
    WaybillTransaction,
    WaybillShipment,
    Manifest,
    ManifestWaybill,
    Delivery
}

module.exports = { resolvers };