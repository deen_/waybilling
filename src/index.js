const { ApolloServer } = require("apollo-server")
const { typeDefs } = require("./schema/type-defs")
const { resolvers } = require("./schema/resolvers")
require('./db/mongoose')

const server = new ApolloServer({ typeDefs, resolvers });

server.listen().then(( {url} ) => {
    console.log(`Server is ready at ${url}`);
})