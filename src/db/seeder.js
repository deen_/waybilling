const fs = require('fs')
require('../db/mongoose')

const Address = require('../models/address')
const Barangay = require('../models/barangay')
const City = require('../models/city')
const Customer = require('../models/customer')
const Delivery = require('../models/delivery')
const Driver = require('../models/driver')
const Item = require('../models/item')
const ManifestWaybill = require('../models/manifest_waybill')
const Manifest = require('../models/manifest')
const Province = require('../models/province')
const Unit = require('../models/unit')
const Vehicle = require('../models/vehicle')
const WaybillShipment = require('../models/waybill_shipment')
const WaybillTransaction = require('../models/waybill_transaction')

const addresses     = JSON.parse( fs.readFileSync(`${__dirname}/_data/addresses.json`, "utf-8") )
const barangays     = JSON.parse( fs.readFileSync(`${__dirname}/_data/barangays.json`, "utf-8") )
const cities        = JSON.parse( fs.readFileSync(`${__dirname}/_data/cities.json`, "utf-8") )
const customers     = JSON.parse( fs.readFileSync(`${__dirname}/_data/customers.json`, "utf-8") )
const deliveries    = JSON.parse( fs.readFileSync(`${__dirname}/_data/deliveries.json`, "utf-8") )
const drivers       = JSON.parse( fs.readFileSync(`${__dirname}/_data/drivers.json`, "utf-8") )
const items         = JSON.parse( fs.readFileSync(`${__dirname}/_data/items.json`, "utf-8") )
const manifest_waybills = JSON.parse( fs.readFileSync(`${__dirname}/_data/manifest_waybills.json`, "utf-8") )
const manifests     = JSON.parse( fs.readFileSync(`${__dirname}/_data/manifests.json`, "utf-8") )
const provinces     = JSON.parse( fs.readFileSync(`${__dirname}/_data/provinces.json`, "utf-8") )
const units         = JSON.parse( fs.readFileSync(`${__dirname}/_data/units.json`, "utf-8") )
const vehicles      = JSON.parse( fs.readFileSync(`${__dirname}/_data/vehicles.json`, "utf-8") )
const waybill_shipments     = JSON.parse( fs.readFileSync(`${__dirname}/_data/waybill_shipments.json`, "utf-8") )
const waybill_transactions  = JSON.parse( fs.readFileSync(`${__dirname}/_data/waybill_transactions.json`, "utf-8") )

const importData = async() => {
    try {
        console.log("Seeding customers...");
        await Customer.deleteMany();
        for(const data of customers ) {
            const customer = new Customer(data)
            await customer.save()
        }

        console.log("Seeding provinces...");
        await Province.deleteMany();
        for(const data of provinces ) {
            const province = new Province(data)
            await province.save()
        }

        console.log("Seeding cities...");
        await City.deleteMany();
        for(const data of cities ) {
            const city = new City(data)
            await city.save()
        }

        console.log("Seeding barangays...");
        await Barangay.deleteMany();
        for(const data of barangays ) {
            const barangay = new Barangay(data)
            await barangay.save()
        }

        console.log("Seeding addresses...");
        await Address.deleteMany();
        for(const data of addresses ) {
            const address = new Address(data)
            await address.save()
        }

        console.log("Seeding drivers...");
        await Driver.deleteMany();
        for(const data of drivers ) {
            const driver = new Driver(data)
            await driver.save()
        }

        console.log("Seeding items...");
        await Item.deleteMany();
        for(const data of items ) {
            const item = new Item(data)
            await item.save()
        }

        console.log("Seeding units...");
        await Unit.deleteMany();
        for(const data of units ) {
            const unit = new Unit(data)
            await unit.save()
        }

        console.log("Seeding vehicles...");
        await Vehicle.deleteMany();
        for(const data of vehicles ) {
            const vehicle = new Vehicle(data)
            await vehicle.save()
        }

        console.log("Seeding waybill transactions...");
        await WaybillTransaction.deleteMany();
        for(const data of waybill_transactions ) {
            const waybill_transaction = new WaybillTransaction(data)
            await waybill_transaction.save()
        }

        console.log("Seeding waybill shipments...");
        await WaybillShipment.deleteMany();
        for(const data of waybill_shipments ) {
            const waybill_shipment = new WaybillShipment(data)
            await waybill_shipment.save()
        }

        console.log("Seeding manifests...");
        await Manifest.deleteMany();
        for(const data of manifests ) {
            const manifest = new Manifest(data)
            await manifest.save()
        }

        console.log("Seeding manifest waybills...");
        await ManifestWaybill.deleteMany();
        for(const data of manifest_waybills ) {
            const manifest_waybill = new ManifestWaybill(data)
            await manifest_waybill.save()
        }

        console.log("Seeding deliveries...");
        await Delivery.deleteMany();
        for(const data of deliveries ) {
            const delivery = new Delivery(data)
            await delivery.save()
        }

        console.log("Seeding completed!");
        process.exit();
    } catch (e) {
        console.log(e);
    }
}

importData();